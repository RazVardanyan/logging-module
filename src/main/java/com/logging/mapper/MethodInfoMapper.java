package com.logging.mapper;

import com.logging.model.MethodInfo;
import java.lang.reflect.Method;

public class MethodInfoMapper {
  public MethodInfo map(Method method) {
    final MethodInfo info = new MethodInfo();
    info.setName(method.getName());
    info.setArgumentTypes(method.getParameterTypes());
    info.setCountOfArguments(method.getParameterCount());
    return info;
  }
}
