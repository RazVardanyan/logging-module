package com.logging.service.impl;

import com.logging.log.PrintLog;
import com.logging.log.PrintLogRegistry;
import com.logging.model.Shape;
import com.logging.service.DrawingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DrawingServiceImpl implements DrawingService {
  private static final Logger LOGGER = LoggerFactory.getLogger(DrawingServiceImpl.class);

  public DrawingServiceImpl(PrintLogRegistry logRegistry) {
    logRegistry.registry(getClass());
  }

  @PrintLog
  @Override
  public void draw(Shape shape) {
    LOGGER.info("IN ORIGINAL DRAW METHOD");
  }

  @Override
  public void remove(Shape shape) {
    LOGGER.info("IN ORIGINAL REMOVE METHOD");
  }
}
