package com.logging.service;

import com.logging.model.Shape;

public interface DrawingService {
  void draw(Shape shape);

  void remove(Shape shape);
}
