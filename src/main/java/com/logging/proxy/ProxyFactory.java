package com.logging.proxy;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logging.log.PrintLogRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Proxy;

public class ProxyFactory {
  private static final Logger LOGGER = LoggerFactory.getLogger(ProxyFactory.class);
  private static final PrintLogRegistry LOG_REGISTRY;
  private static final ObjectMapper MAPPER;

  static {
    LOG_REGISTRY = PrintLogRegistry.getInstance();
    MAPPER = new ObjectMapper();
  }

  private ProxyFactory() {
    throw new IllegalArgumentException();
  }

  public static <T> T getProxy(T target) {
    final var clazz = target.getClass();
    return (T) Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(),
        (proxy, method, args) -> {
          if (args == null) {
            return method.invoke(target);
          }
          if (!LOG_REGISTRY.isMethodAnnotated(clazz, method)) {
            return method.invoke(target, args);
          }
          String argumentsAsJson = MAPPER.writeValueAsString(args);
          LOGGER.info("{} METHOD FROM {} CLASS, ARGUMENTS AS JSON FORMAT: {}", method.getName(), clazz.getSimpleName(), argumentsAsJson);
          return method.invoke(target, args);
        });
  }
}
