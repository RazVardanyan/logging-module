package com.logging;

import com.logging.log.PrintLogRegistry;
import com.logging.model.Shape;
import com.logging.proxy.ProxyFactory;
import com.logging.service.DrawingService;
import com.logging.service.impl.DrawingServiceImpl;

public class LoggingModuleApplication {
  public static void main(String[] args) {
    PrintLogRegistry logRegistry = PrintLogRegistry.getInstance();
    DrawingService o = ProxyFactory.getProxy(new DrawingServiceImpl(logRegistry));
    Shape shape = new Shape("RECTANGLE",200,300);
    o.draw(shape);
    o.remove(shape);
  }
}