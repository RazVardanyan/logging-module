package com.logging.log;

import com.logging.mapper.MethodInfoMapper;
import com.logging.model.MethodInfo;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

public final class PrintLogRegistry {
  private static final ThreadLocal<PrintLogRegistry> THREAD_LOCAL_INSTANCE;
  private final ConcurrentHashMap<Class<?>, List<MethodInfo>> registryStorage;
  private final MethodInfoMapper methodInfoMapper;

  static {
    THREAD_LOCAL_INSTANCE = ThreadLocal.withInitial(PrintLogRegistry::new);
  }

  private PrintLogRegistry() {
    registryStorage = new ConcurrentHashMap<>();
    methodInfoMapper = new MethodInfoMapper();
  }

  public void registry(Class<?> clazz) {
    List<MethodInfo> methodNamesWithSpecificAnnotation = Arrays.stream(clazz.getMethods())
        .filter(method -> {
          PrintLog printLog = method.getAnnotation(PrintLog.class);
          return printLog != null;
        })
        .map(methodInfoMapper::map)
        .collect(Collectors.toList());
    registryStorage.put(clazz, methodNamesWithSpecificAnnotation);
  }

  public boolean isMethodAnnotated(Class<?> clazz, Method method) {
    List<MethodInfo> methods = getClassMethods(clazz);
    MethodInfo info = methodInfoMapper.map(method);
    return methods.contains(info);
  }

  public static PrintLogRegistry getInstance() {
    PrintLogRegistry logRegistry = THREAD_LOCAL_INSTANCE.get();
    if (logRegistry == null)
      throw new RuntimeException();
    return THREAD_LOCAL_INSTANCE.get();
  }

  public static void removeInstance() {
    THREAD_LOCAL_INSTANCE.remove();
  }

  public List<MethodInfo> getClassMethods(Class<?> clazz) {
    return registryStorage.get(clazz);
  }

  public ConcurrentMap<Class<?>, List<MethodInfo>> getRegistryStorage() {
    return registryStorage;
  }
}
