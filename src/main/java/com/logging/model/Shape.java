package com.logging.model;

public class Shape {
  private String name;
  private int height;
  private int width;

  public Shape(String name, int height, int width) {
    this.name = name;
    this.height = height;
    this.width = width;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }
}
