package com.logging.model;

import java.util.Arrays;
import java.util.Objects;

public class MethodInfo {
  private String name;
  private int countOfArguments;
  private Class<?>[] argumentTypes;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCountOfArguments() {
    return countOfArguments;
  }

  public void setCountOfArguments(int countOfArguments) {
    this.countOfArguments = countOfArguments;
  }

  public Class<?>[] getArgumentTypes() {
    return argumentTypes;
  }

  public void setArgumentTypes(Class<?>[] argumentTypes) {
    this.argumentTypes = argumentTypes;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MethodInfo info = (MethodInfo) o;
    return countOfArguments == info.countOfArguments && Objects.equals(name, info.name) && Arrays.equals(argumentTypes, info.argumentTypes);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(name, countOfArguments);
    result = 31 * result + Arrays.hashCode(argumentTypes);
    return result;
  }
}
